"""
Test fixures.

Created tables are unlogged for performance during generative testing.
"""
from pathlib import Path
from os import environ
import psycopg2
import pytest

DBREWIND_CODE = (Path(__file__).parent.parent / 'sql' / 'dbrewind.sql').read_text()


@pytest.fixture
def db_conn():
    """Set up the test database and get test database connection."""
    host = environ.get('PGHOST', 'localhost')
    conn = psycopg2.connect(
        f"dbname=dbrewind_test user=dbrewind_test password=dbrewind host={host}")
    with conn.cursor() as cur:
        cur.execute('DROP SCHEMA IF EXISTS dbrewind CASCADE')
        cur.execute(DBREWIND_CODE)
        cur.execute('COMMIT')
    yield conn
    with conn.cursor() as cur:
        cur.execute('ROLLBACK')
        cur.execute('SELECT dbrewind.uninstall()')


@pytest.fixture
def db_schema(db_conn):
    """Set up test tables."""
    drop_statements = '''
DROP TABLE IF EXISTS table_references;
DROP TABLE IF EXISTS table_pk;
DROP TABLE IF EXISTS table_unique;
COMMIT;'''

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)

    with db_conn.cursor() as cur:
        cur.execute('''
CREATE UNLOGGED TABLE table_pk (
    col1 TEXT PRIMARY KEY,
    col2 TEXT NOT NULL
);
CREATE UNLOGGED TABLE table_unique (
    col1 TEXT NOT NULL,
    col2 TEXT NOT NULL,
    UNIQUE(col1, col2)
);
CREATE UNLOGGED TABLE table_references (
    col1 TEXT REFERENCES table_pk (col1) ON DELETE CASCADE NOT NULL,
    col2 JSON, -- Non-comparable type but it should still work.
    UNIQUE(col1)
);
COMMIT;''')

    yield ['table_pk', 'table_unique', 'table_references']

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)


@pytest.fixture
def db_schema_sequences(db_conn):
    """Set up test tables and sequences."""
    drop_statements = '''
DROP TABLE IF EXISTS table_pk_seq_1, table_pk_seq_2;
DROP SEQUENCE IF EXISTS
    seq_regular,
    seq_increment_4,
    seq_increment_minus_4,
    seq_descend_from_100_by_2;
COMMIT;'''

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)

    with db_conn.cursor() as cur:
        cur.execute('''
CREATE UNLOGGED TABLE table_pk_seq_1 (
    col1 SERIAL PRIMARY KEY,
    col2 TEXT NOT NULL
);
CREATE UNLOGGED TABLE table_pk_seq_2 (
    col1 SERIAL PRIMARY KEY,
    col2 TEXT NOT NULL
);
CREATE SEQUENCE seq_regular;
CREATE SEQUENCE seq_increment_4 INCREMENT BY 4;
CREATE SEQUENCE seq_increment_minus_4 INCREMENT BY -4;
CREATE SEQUENCE seq_descend_from_100_by_2 INCREMENT BY -2 MAXVALUE 100 MINVALUE -1;
COMMIT;
''')

    yield {
        'tables': ['table_pk_seq_1', 'table_pk_seq_2'],
        'seqs': [
            'seq_regular',
            'seq_increment_4',
            'seq_increment_minus_4',
            'seq_descend_from_100_by_2',
            'table_pk_seq_1_col1_seq',
            'table_pk_seq_2_col1_seq',
        ],
    }

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)


@pytest.fixture
def db_bad_schema(db_conn):
    """Create a table with no defined uniqueness, which cannot be rewound by dbrewind."""
    drop_statements = '''
DROP TABLE IF EXISTS table_nonunique;
COMMIT;'''

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)

    with db_conn.cursor() as cur:
        cur.execute('''
CREATE TABLE table_nonunique (
    col1 TEXT,
    col2 TEXT NOT NULL
);
COMMIT;''')

    yield ['table_nonunique']

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)


@pytest.fixture(autouse=True)
def db_schema_confusion(db_conn):
    """Create tables in the public schema names identically as the ones in the dbrewind schema."""
    drop_statements = '''
DROP TABLE IF EXISTS sequences;
DROP TABLE IF EXISTS metadata;
COMMIT;'''

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)

    with db_conn.cursor() as cur:
        cur.execute('''
CREATE TABLE sequences (id SERIAL PRIMARY KEY);
CREATE TABLE metadata (id SERIAL PRIMARY KEY);
COMMIT;''')

    yield

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)
