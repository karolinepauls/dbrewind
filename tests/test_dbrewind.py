"""Basic test suite."""
from datetime import timedelta
from enum import Enum
import string
import pytest
from psycopg2.errors import RaiseException
from psycopg2.sql import SQL, Identifier
from psycopg2.extras import Json
from hypothesis import HealthCheck, Phase, settings, given
from hypothesis.strategies import (
    data, integers, tuples, lists, text, dictionaries, sampled_from, sets, composite,
)


class Statement(Enum):
    """Supported SQL statement."""
    INSERT = 'INSERT'
    DELETE = 'DELETE'
    UPDATE = 'UPDATE'


def capture_state(tables, db_conn):
    """Dump tables."""
    state = {}
    with db_conn.cursor() as cur:
        for table in tables:
            table_ident = Identifier(table)
            cur.execute(
                SQL(
                    'SELECT * FROM {table} ORDER BY {table}.*::TEXT'
                ).format(table=table_ident))
            state[table] = (cur.fetchall())
    return state


def test_licence(db_conn, db_schema):
    """Test the licence notice function."""
    with db_conn.cursor() as cur:
        cur.execute('SELECT dbrewind.licence()')
        [licence] = cur.fetchone()
        assert licence == '''dbrewind

Copyright (C) 2021- Karoline C. Pauls

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.'''


@pytest.mark.parametrize('commit', [True, False])
def test_install_rewind(db_conn, db_schema, commit):
    """Test adding rows (INSERT, UPDATE, DELETE) to sample tables an rewinding."""
    with db_conn.cursor() as cur:
        cur.execute('''
INSERT INTO table_pk VALUES
    ('a', 'b'),
    ('c', 'd');

INSERT INTO table_unique VALUES
    ('a', 'b'),
    ('c', 'd');

INSERT INTO table_references VALUES
    ('a', '{}'),
    ('c', '{"a": 20}');
''')
        if commit:
            cur.execute('COMMIT')

        state_init = capture_state(db_schema, db_conn)
        cur.execute('SELECT dbrewind.install()')
        state_installed = capture_state(db_schema, db_conn)
        assert state_installed == state_init

        cur.execute('''
INSERT INTO table_pk VALUES
    ('aa', 'bb'),
    ('cc', 'dd');
UPDATE table_pk SET col2 = 'bbbbbb' WHERE col1 = 'aa';

INSERT INTO table_unique VALUES
    ('aa', 'bb'),
    ('cc', 'dd');

DELETE FROM table_unique WHERE col1 = 'aa';

INSERT INTO table_references VALUES
    ('aa', '{}'),
    ('cc', '{"a": 20}');
''')
        if commit:
            cur.execute('COMMIT')

        cur.execute('DELETE FROM table_unique WHERE col1 = \'bb\';')
        cur.execute('UPDATE table_pk SET col2 = \'dddddd\' WHERE col1 = \'cc\';')
        if commit:
            cur.execute('COMMIT')

        cur.execute('SELECT dbrewind.rewind()')
        if commit:
            cur.execute('COMMIT')
        state_rewound = capture_state(db_schema, db_conn)
        assert state_rewound == state_init, 'Rewound to init'


def test_rewind_updates_preexisting(db_conn, db_schema):
    """
    Test rewinding UPDATEs to records existing prior to dbrewind installation.

    This is a regression test for a bug found present up to commit 56a750d.
    """
    with db_conn.cursor() as cur:
        cur.execute('''
INSERT INTO table_pk VALUES
    ('a', 'b'),
    ('c', 'd');

SELECT dbrewind.install();
''')
        state_init = capture_state(db_schema, db_conn)
        cur.execute('UPDATE table_pk SET col1 = \'aa\' WHERE col1 = \'a\'')
        cur.execute('SELECT dbrewind.rewind()')
        state_rewound = capture_state(db_schema, db_conn)
        assert state_rewound == state_init, 'Rewound to init'


def test_rewind_update_non_pk(db_conn, db_schema):
    """
    Test rewinding UPDATEs to non-PK fields of records existing prior to dbrewind installation.
    """
    with db_conn.cursor() as cur:
        cur.execute('''
INSERT INTO table_pk VALUES ('a', 'b');

SELECT dbrewind.install();
''')
        state_init = capture_state(db_schema, db_conn)
        cur.execute('UPDATE table_pk SET col2 = \'bb\' WHERE col1 = \'a\'')
        cur.execute('SELECT dbrewind.rewind()')
        state_rewound = capture_state(db_schema, db_conn)
        assert state_rewound == state_init, 'Rewound to init'


def test_rewind_updates_deleted_prior(db_conn, db_schema):
    """
    Test rewinding UPDATEs that effectively re-insert previously deleted rows.

    This is a regression test for a bug found present up to commit 56a750d.

    While this test doesn't manage to produce inconsistent rewind result, it manages to make diffs
    overlap (in the buggy version of the code), which is bad enough and should not be the case.
    """
    with db_conn.cursor() as cur:
        cur.execute('''
INSERT INTO table_pk VALUES
    ('a', 'b'),
    ('c', 'd');
''')
        cur.execute('SELECT dbrewind.install();')
        cur.execute('DELETE FROM table_pk WHERE col1 = \'a\'')
        cur.execute('UPDATE table_pk SET col1 = \'a\', col2 = \'b\' WHERE col1 = \'c\'')


def test_multi_column_uniqueness(db_conn, db_schema):
    """Ensure dbrewind's understanding of uniqueness reflects the one of the table."""
    with db_conn.cursor() as cur:
        cur.execute('''
SELECT dbrewind.install();

-- table_unique has its 2 columns "unique together"
INSERT INTO table_unique VALUES
    ('aa', 'bb'),
    ('aa', 'dd');
''')

        cur.execute('''SELECT dbrewind.unique_columns('table_unique')''')
        unique_columns = cur.fetchone()[0]
        assert unique_columns == ['col1', 'col2']

        cur.execute('''SELECT dbrewind.unique_columns('table_pk')''')
        unique_columns_pk_table = cur.fetchone()[0]
        assert unique_columns_pk_table == ['col1']


def test_bad_schema(db_bad_schema, db_conn):
    """Ensure we raise an exception if the schema is incompatible."""
    with pytest.raises(RaiseException) as e:
        with db_conn.cursor() as cur:
            cur.execute('SELECT dbrewind.install()')
    assert 'INCOMPATIBLE SCHEMA' in e.value.args[0]


def capture_sequence_state(sequences, db_conn):
    state = {}
    with db_conn.cursor() as cur:
        for seq in sequences:
            seq_id = Identifier(seq)
            cur.execute(SQL('SELECT last_value, is_called is_called FROM {seq}').format(seq=seq_id))
            state[seq] = cur.fetchone()
    return state


def test_metadata_table_reset(db_conn, db_schema):
    """Ensure the `modified` field in dbrewind.metadata gets reset.."""
    with db_conn.cursor() as cur:
        cur.execute('SELECT dbrewind.install()')

        def is_modified(table):
            cur.execute("SELECT modified FROM dbrewind.metadata WHERE table_name = %s", (table,))
            [modified] = cur.fetchone()
            return modified

        assert is_modified('table_pk') is False
        assert is_modified('table_unique') is False

        cur.execute("INSERT INTO table_pk VALUES ('a', 'b')")
        assert is_modified('table_pk') is True
        assert is_modified('table_unique') is False

        cur.execute('SELECT dbrewind.rewind()')
        assert is_modified('table_pk') is False
        assert is_modified('table_unique') is False


@pytest.mark.parametrize('install_sql', [
    'SELECT dbrewind.install(true)',
    'SELECT dbrewind.install()',
])
def test_rewind_sequences(db_conn, db_schema_sequences, install_sql):
    """Test restoring sequences while rewinding the database."""
    with db_conn.cursor() as cur:
        # Insert 2 vals to table 1 before installing.
        cur.execute("INSERT INTO table_pk_seq_1 (col2) VALUES ('a'), ('b')")

        seq_state_init = capture_sequence_state(db_schema_sequences['seqs'], db_conn)
        table_state_init = capture_state(db_schema_sequences['tables'], db_conn)

        cur.execute(install_sql)

        for _ in range(10):
            for seq in db_schema_sequences['seqs']:
                cur.execute('SELECT nextval(%s)', (seq,))

        cur.execute("INSERT INTO table_pk_seq_2 (col2) VALUES ('aa'), ('bb')")

        cur.execute('SELECT dbrewind.rewind()')

        table_state_rewound = capture_state(db_schema_sequences['tables'], db_conn)
        sequence_state_rewound = capture_sequence_state(db_schema_sequences['seqs'], db_conn)
        assert table_state_rewound == table_state_init, 'Rewound tables to init'
        assert sequence_state_rewound == seq_state_init, 'Restored sequence state'

        cur.execute("INSERT INTO table_pk_seq_1 (col2) VALUES ('111') RETURNING col1")
        [table_1_generated_id] = cur.fetchone()
        assert table_1_generated_id == 3, (
            "Table 1's PK sequence should be reset to its state before installing")

        cur.execute("INSERT INTO table_pk_seq_2 (col2) VALUES ('222') RETURNING col1")
        [table_2_generated_id] = cur.fetchone()
        assert table_2_generated_id == 1, "Table 2's PK sequence should be reset to start."


def test_dont_rewind_sequences(db_conn, db_schema_sequences):
    """Test not restoring sequences while rewinding the database if the feature is disabled."""
    with db_conn.cursor() as cur:
        cur.execute('SELECT dbrewind.install(false)')

        seq_state_init = capture_sequence_state(db_schema_sequences['seqs'], db_conn)
        table_state_init = capture_state(db_schema_sequences['tables'], db_conn)

        direct_increments = 10
        insertions = 5

        cur.executemany("INSERT INTO table_pk_seq_1 (col2) VALUES (%s)",
                        [(str(n),) for n in range(insertions)])

        for _ in range(direct_increments):
            for seq in db_schema_sequences['seqs']:
                cur.execute('SELECT nextval(%s)', (seq,))

        sequence_state_progressed = capture_sequence_state(db_schema_sequences['seqs'], db_conn)
        cur.execute('SELECT dbrewind.rewind()')

        table_state_rewound = capture_state(db_schema_sequences['tables'], db_conn)
        sequence_state_rewound = capture_sequence_state(db_schema_sequences['seqs'], db_conn)
        assert table_state_rewound == table_state_init, 'Rewound tables to init'
        assert sequence_state_rewound != seq_state_init, 'Did not restore sequence state'
        assert sequence_state_rewound == sequence_state_progressed, 'Sequence state left unchanged'

        cur.execute("SELECT COUNT(*) FROM table_pk_seq_1")
        [table_1_rows] = cur.fetchone()
        assert table_1_rows == 0, 'Table 1 should be rewound to init, when it contained no rows.'

        cur.execute("INSERT INTO table_pk_seq_1 (col2) VALUES ('111') RETURNING col1")
        [table_1_generated_id] = cur.fetchone()
        assert table_1_generated_id == direct_increments + insertions + 1, (
            "Table 1's PK sequence state should not be restored")


def indexes(length):
    return lists(
        integers(min_value=0, max_value=max(0, length - 1)),
        max_size=length, unique=True,
    )


# We are not testing Postgres or psycopg2, let's generate human-readable data so failures are easier
# to analyse.
simple_text = text(alphabet=string.ascii_letters)


@pytest.mark.slow
@given(data())
@settings(
    suppress_health_check=(HealthCheck.function_scoped_fixture,),
    # Some iterations can take ~600ms. We're not using the database for what it's been designed.
    deadline=timedelta(seconds=10),
    max_examples=200,
)
def test_rewind_relation_generated(db_conn, db_schema, data):
    """
    Test rewinding sets of insertions to and deletions to several tables.

    2 of the tables are related to each other (FK) and one of them contains a non-comparable JSON
    column.
    """
    draw = data.draw
    with db_conn.cursor() as cur:
        cur.execute('TRUNCATE table_pk, table_references, table_unique')
        cur.execute('SELECT dbrewind.uninstall()')

        table_pk_insertions_col1 = draw(lists(simple_text, unique=True),
                                        label='table_pk_insertions_col1')
        table_pk_insertions_count = len(table_pk_insertions_col1)

        table_pk_insertions_col2 = draw(
            lists(
                simple_text,
                min_size=table_pk_insertions_count,
                max_size=table_pk_insertions_count,
            ),
            label='table_pk_insertions_col2',
        )
        table_pk_insertions = list(zip(table_pk_insertions_col1, table_pk_insertions_col2))

        table_unique_insertions = draw(
            lists(tuples(simple_text, simple_text), unique=True),
            label='table_unique_insertions',
        )

        # Table referencing table_pk in its first column.
        table_references_col2_insertions = draw(
            lists(
                dictionaries(simple_text, integers()),
                min_size=table_pk_insertions_count,
                max_size=table_pk_insertions_count,
            ),
            label='table_references_col2_insertions',
        )
        table_references_insertions = list(zip(
            table_pk_insertions_col1,
            map(Json, table_references_col2_insertions)))

        table_pk_deletions = draw(indexes(len(table_pk_insertions)), label='table_pk_deletions')
        table_unique_deletions = draw(indexes(len(table_unique_insertions)),
                                      label='table_unique_deletions')
        table_references_deletions = draw(indexes(len(table_references_insertions)),
                                          label='table_references_deletions')

        cur.execute('SELECT dbrewind.install()')
        state_init = capture_state(db_schema, db_conn)

        cur.executemany('INSERT INTO table_pk VALUES (%s, %s)', table_pk_insertions)
        cur.executemany('INSERT INTO table_unique VALUES (%s, %s)', table_unique_insertions)
        cur.executemany('INSERT INTO table_references VALUES (%s, %s)',
                        table_references_insertions)

        cur.executemany(
            'DELETE FROM table_pk WHERE (col1, col2) = (%s, %s)',
            [table_pk_insertions[i] for i in table_pk_deletions],
        )
        cur.executemany(
            'DELETE FROM table_unique WHERE (col1, col2) = (%s, %s)',
            [table_unique_insertions[i] for i in table_unique_deletions],
        )
        cur.executemany(
            'DELETE FROM table_references WHERE col1 = %s',
            [(table_pk_insertions_col1[i], ) for i in table_references_deletions],
        )

        cur.execute('SELECT dbrewind.rewind()')
        state_rewound = capture_state(db_schema, db_conn)
        assert state_rewound == state_init


@pytest.mark.slow
@given(data())
@settings(
    suppress_health_check=(HealthCheck.function_scoped_fixture,),
    # Some iterations can take ~600ms. We're not using the database for what it's been designed.
    deadline=timedelta(seconds=10),
    max_examples=200,
    # Hypothesis isn't great at shrinking this.
    phases=[Phase.explicit, Phase.reuse, Phase.generate],
)
def test_rewind_history_generated_unique(db_conn, db_schema, data):
    """
    Test rewinding a generated, complex change history of a single table with a UNIQUE constraint.

    The constraint includes all table columns.
    """
    draw = data.draw
    with db_conn.cursor() as cur:
        cur.execute('DELETE FROM table_unique')
        cur.execute('SELECT dbrewind.uninstall()')

        current_records = set()
        new_rows = tuples(simple_text, simple_text).filter(lambda t: t not in current_records)

        def existing_rows():
            return sampled_from(sorted(current_records))

        # Generate an initial state (to be later rewound to).
        initial_records = draw(sets(new_rows), label='initial_records')
        cur.executemany('INSERT INTO table_unique VALUES (%s, %s)', initial_records)
        current_records.update(initial_records)
        state_init = capture_state(db_schema, db_conn)

        cur.execute('SELECT dbrewind.install()')

        for _ in range(draw(integers(max_value=100))):
            possible_ops = [Statement.INSERT] + (
                [Statement.UPDATE, Statement.DELETE] if current_records else [])
            operation = draw(sampled_from(possible_ops))

            if operation == Statement.INSERT:
                insertions = draw(sets(new_rows), label='insertions')
                cur.executemany('INSERT INTO table_unique VALUES (%s, %s)', insertions)
                current_records.update(insertions)
            elif operation == Statement.DELETE:
                deletions = draw(sets(existing_rows()), label='deletions')
                cur.executemany(
                    'DELETE FROM table_unique WHERE (col1, col2) = (%s, %s)',
                    deletions,
                )
                current_records.difference_update(deletions)
            elif operation == Statement.UPDATE:
                updates_where = draw(sets(existing_rows()), label='update_where')
                updates_count = len(updates_where)
                updates_set = draw(sets(new_rows, min_size=updates_count, max_size=updates_count),
                                   label='update_set')

                # Set/map iteration now preserves order.
                for update_where, update_set in zip(updates_where, updates_set):
                    # This update is really, a "replace" operation, since no fields are kept.
                    # `test_rewind_history_generated_pk` does a real update.
                    cur.execute(
                        'UPDATE table_unique SET col1 = %s, col2 = %s '
                        'WHERE (col1, col2) = (%s, %s)',
                        update_where + update_set,
                    )

                current_records.update(updates_where)
                current_records.difference_update(updates_set)

        cur.execute('SELECT dbrewind.rewind()')
        state_rewound = capture_state(db_schema, db_conn)
        assert state_rewound == state_init


@pytest.mark.slow
@given(data())
@settings(
    suppress_health_check=(HealthCheck.function_scoped_fixture,),
    # Some iterations can take ~600ms. We're not using the database for what it's been designed.
    deadline=timedelta(seconds=10),
    max_examples=200,
    # Hypothesis isn't great at shrinking this.
    phases=[Phase.explicit, Phase.reuse, Phase.generate],
)
def test_rewind_history_generated_pk(db_conn, db_schema, data):
    """
    Test rewinding a generated, complex change history of a table with a PK and another column.

    In this test one column is the identifier, while the other is a non-ID value.
    """
    draw = data.draw
    with db_conn.cursor() as cur:
        cur.execute('SELECT dbrewind.uninstall()')
        cur.execute('DELETE FROM table_pk')

        current_ids = set()

        @composite
        def new_rows(draw):
            ids = draw(lists(simple_text.filter(lambda t: t not in current_ids), unique=True))
            vals = draw(lists(simple_text))
            return tuple(zip(ids, vals))

        def existing_ids():
            return sampled_from(sorted(current_ids))

        # Generate an initial state (to be later rewound to).
        initial_records = draw(new_rows(), label='initial_records')
        cur.executemany('INSERT INTO table_pk VALUES (%s, %s)', initial_records)
        current_ids.update(k for k, _ in initial_records)
        state_init = capture_state(db_schema, db_conn)

        cur.execute('SELECT dbrewind.install()')

        for _ in range(draw(integers(max_value=100))):
            possible_ops = [Statement.INSERT] + (
                [Statement.UPDATE, Statement.DELETE] if current_ids else [])
            operation = draw(sampled_from(possible_ops))

            if operation == Statement.INSERT:
                insertions = draw(new_rows(), label='insertions')
                cur.executemany('INSERT INTO table_pk VALUES (%s, %s)', insertions)
                current_ids.update(k for k, _ in insertions)
            elif operation == Statement.DELETE:
                deletions = draw(lists(existing_ids(), min_size=1, unique=True), label='deletions')
                cur.executemany(
                    'DELETE FROM table_pk WHERE col1 = %s',
                    [(deletion,) for deletion in deletions],
                )
                current_ids.difference_update(deletions)
            elif operation == Statement.UPDATE:
                updates_where = draw(lists(existing_ids(), min_size=1, unique=True),
                                     label='update_where')
                updates_count = len(updates_where)
                updates_set = draw(lists(simple_text,
                                         min_size=updates_count, max_size=updates_count),
                                   label='update_set')

                for update_where, update_set in zip(updates_where, updates_set):
                    cur.execute(
                        'UPDATE table_pk SET col2 = %s WHERE col1 = %s',
                        (update_set, update_where),
                    )

        cur.execute('SELECT dbrewind.rewind()')
        state_rewound = capture_state(db_schema, db_conn)
        assert state_rewound == state_init
