"""Benchmarking."""
import pytest
from psycopg2.sql import SQL, Identifier
from psycopg2.extras import execute_values


TABLE_COUNT = 50
WARMUP_ROUNDS = 5
ROUNDS = 15


@pytest.fixture
def large_schema(db_conn):
    """
    DB schema with many tables.

    Doesn't conflict with other schema fixtures.
    """
    table_names = [f'table_{i}' for i in range(TABLE_COUNT)]

    drop_statements = ';\n'.join(f'DROP TABLE IF EXISTS {t}' for t in table_names)
    create_statements = ';\n'.join(
        f'CREATE TABLE {t} (id SERIAL PRIMARY KEY, a TEXT, b TEXT, c TEXT, d TEXT, e TEXT, f TEXT)'
        for t in table_names)

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)
        cur.execute(create_statements)
        cur.execute('COMMIT')

    yield table_names

    with db_conn.cursor() as cur:
        cur.execute(drop_statements)
        cur.execute('COMMIT')


@pytest.mark.slow
def test_rewind_no_data(benchmark, db_conn, large_schema):
    with db_conn.cursor() as cur:
        cur.execute('SELECT dbrewind.install()')
        benchmark(cur.execute, 'SELECT dbrewind.rewind()')


def populate_tables(cur, tables):
    for table in tables:
        execute_values(
            cur,
            SQL('INSERT INTO {table} (a, b, c, d) VALUES %s').format(table=Identifier(table)),
            [('a', 'b', 'c', 'd')] * 20,
        )


@pytest.mark.slow
@pytest.mark.parametrize('modified_tables', [1, 5, 15])
def test_rewind_data_in_some_tables(benchmark, db_conn, large_schema, modified_tables):
    assert modified_tables < TABLE_COUNT

    with db_conn.cursor() as cur:
        cur.execute('SELECT dbrewind.install()')

        def setup():
            populate_tables(cur, large_schema[:modified_tables])

        benchmark.pedantic(
            lambda: cur.execute('SELECT dbrewind.rewind()'),
            setup=setup,
            warmup_rounds=WARMUP_ROUNDS,
            rounds=ROUNDS,
        )


@pytest.mark.slow
def test_rewind_data_in_all_tables(benchmark, db_conn, large_schema):
    with db_conn.cursor() as cur:
        cur.execute('SELECT dbrewind.install()')

        def setup():
            populate_tables(cur, large_schema)

        benchmark.pedantic(
            lambda: cur.execute('SELECT dbrewind.rewind()'),
            setup=setup,
            warmup_rounds=WARMUP_ROUNDS,
            rounds=ROUNDS,
        )


@pytest.mark.slow
def test_insert_performance_baseline(benchmark, db_conn, large_schema):
    """Measure performance of table insertion WITHOUT dbrewind."""
    with db_conn.cursor() as cur:
        def setup():
            cur.execute('TRUNCATE ' + ','.join(large_schema))

        benchmark.pedantic(
            lambda: populate_tables(cur, large_schema),
            setup=setup,
            warmup_rounds=WARMUP_ROUNDS,
            rounds=ROUNDS,
        )


@pytest.mark.slow
def test_insert_performance_with_dbrewind(benchmark, db_conn, large_schema):
    """Measure performance of table insertion WITH dbrewind."""
    with db_conn.cursor() as cur:

        def setup():
            cur.execute('SELECT dbrewind.uninstall()')
            cur.execute('TRUNCATE ' + ','.join(large_schema))
            cur.execute('SELECT dbrewind.install()')

        benchmark.pedantic(
            lambda: populate_tables(cur, large_schema),
            setup=setup,
            warmup_rounds=WARMUP_ROUNDS,
            rounds=ROUNDS,
        )
