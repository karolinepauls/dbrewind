# dbrewind

Possibly a holy grail of database cleanup between tests. Trigger-based. Can undo inserts, updates,
and deletes, as well as sequence advancement. Does not support DDL and TRUNCATE.

The library is purely in PL/pgSQL and contained entirely within the sql/dbrewind.sql file. All
Python code is exclusively for testing. The project may be automatically identified as Python
because there is now more testing code than library code.

The project is usable, however it isn't used in any test suite, other than the one here. Write-up:
https://karolinepauls.com/articles/dbrewind.html.

Currently it requires all tables to have a notion of uniqueness defined for them (a PK or a unique
constraint on one or more non-null columns). One way to get around it may be to use OIDs for tables
that don't have suitable constraints, adding which shouldn't be a destructive schema change in test
databases.

## Usage

Evaluate the [dbrewind.sql file](sql/dbrewind.sql) in the database as the superuser. Later:

```sql
-- Create and populate your public schema. Then when ready, install dbrewind (it will store its data
-- in the "dbrewind" schema). This would typically be done in a test fixture.
-- Pass `false` to skip restoring sequence state when rewinding.
SELECT dbrewind.install();

-- Make changes to the database. This would typically be test code.

-- Ask dbrewind to rewind. This would typically be the cleanup (post test) phase of the fixture.
-- Close all transactions before rewinding, or they will see stale data. Rewinding must be done by
-- a superuser.
SELECT dbrewind.rewind();

-- You can remove all dbrewind data at any moment. This won't change the public schema, however it
-- will make it impossible to rewind. If you re-install dbrewind at any point, that point will
-- become the next rewind target.
SELECT dbrewind.uninstall();
```

dbrewind installs its functions and does its bookkeeping in the `dbrewind` schema.

It's not recommended to create a `dbrewind` user, since in Postgres the default search path is
`"$user", public`, which in effect makes the `dbrewind` user create or look up objects in the
`dbrewind` schema first.

## Licence

The [dbrewind.sql file](sql/dbrewind.sql) file is available under [Mozilla Public License
2.0](https://mozilla.org/MPL/2.0/), without the "Incompatible With Secondary Licenses" notice.

The other files in this repository, including SQL examples, tests and documentation, are available
under the [MIT License](https://mit-license.org/).

The external resources, including but not limited to the
[write-up](https://karolinepauls.com/articles/dbrewind.html), are available under their respective
licences.

## Development

Requirements: postgresql, virtualenv (recommended).

Create a `dbrewind_test` superuser with a "dbrewind" password for testing.

```
sudo -u postgres -- psql -f examples/setup_user.sql
sudo -u postgres createdb dbrewind_test
```

```
pip install -r ./tests/requirements_test.txt
pytest -v -m 'not slow'
pytest -v -m 'slow'
```

Some tests take time to run due to the use of property-based testing (somewhat incorrectly called
"fuzzing").
