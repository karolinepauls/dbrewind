--- dbrewind
---
--- Copyright (C) 2021- Karoline C. Pauls
---
--- This Source Code Form is subject to the terms of the Mozilla Public
--- License, v. 2.0. If a copy of the MPL was not distributed with this
--- file, You can obtain one at https://mozilla.org/MPL/2.0/.


CREATE SCHEMA IF NOT EXISTS dbrewind;


CREATE OR REPLACE FUNCTION dbrewind.licence() RETURNS TEXT AS $$
    BEGIN
        RETURN 'dbrewind

Copyright (C) 2021- Karoline C. Pauls

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.';
    END;
$$ LANGUAGE plpgsql;


-- Find the smallest set of mutually unique, non-null columns for the table.
-- If there is more than one set of unique non-null columns of equal lowest length, the first is
-- chosen, alphabetically.
CREATE OR REPLACE FUNCTION dbrewind.unique_columns(TEXT) RETURNS TEXT[] AS $$
    DECLARE tbl_name ALIAS FOR $1;
            constraints TEXT[];
    BEGIN
        WITH unique_constraints AS (
            SELECT array_agg(col.column_name::TEXT ORDER BY col.ordinal_position) AS columns
            FROM pg_catalog.pg_constraint con
            INNER JOIN pg_catalog.pg_class     rel ON rel.oid = con.conrelid
            INNER JOIN pg_catalog.pg_namespace nsp ON nsp.oid = con.connamespace
            INNER JOIN information_schema.columns col
                       ON col.ordinal_position = ANY(con.conkey)
                      AND nsp.nspname = col.table_schema
                      AND rel.relname = col.table_name
            WHERE nsp.nspname = 'public'
                 AND rel.relname = tbl_name
                 AND con.contype in ('p', 'u')
            GROUP BY con.connamespace, con.conname
            HAVING bool_or(col.is_nullable::BOOLEAN) = false
        )
        SELECT columns
        FROM unique_constraints
        ORDER BY array_length(columns, 1), columns
        LIMIT 1
        INTO constraints;

        RETURN constraints;
    END;
$$ LANGUAGE plpgsql;


-- Format an array of columns, like {'col1', 'col2'} as a string like 'tbl.col1, table.col2' for
-- table_name equal to 'tbl'.
-- No table prefix if table_name is null or empty.
CREATE OR REPLACE FUNCTION dbrewind.format_columns(TEXT, TEXT[]) RETURNS TEXT AS $$
    DECLARE table_name ALIAS FOR $1;
            columns ALIAS FOR $2;
            columns_text TEXT;
    BEGIN
        table_name = coalesce(table_name, '');
        IF table_name != '' THEN
            table_name = table_name || '.';
        END IF;

        WITH cols AS (SELECT table_name || quote_ident(unnest(columns)) as col)
        SELECT string_agg(col, ', ') FROM cols
        INTO columns_text;

        RETURN columns_text;
    END;
$$ LANGUAGE plpgsql IMMUTABLE;


-- Update diff tables on row change.
CREATE OR REPLACE FUNCTION dbrewind.row_modified() RETURNS trigger AS $$
    DECLARE origin_table TEXT;
            insertions_table TEXT;
            deletions_table TEXT;
            deleted_count INTEGER;
            unique_columns TEXT[];
            unique_columns_row TEXT;
            unique_columns_diff_table TEXT;
            unique_columns_no_prefix TEXT;
    BEGIN
        origin_table = quote_ident(TG_TABLE_SCHEMA) || '.' || quote_ident(TG_TABLE_NAME);
        insertions_table = 'records_inserted_' || TG_TABLE_NAME;
        deletions_table = 'records_deleted_' || TG_TABLE_NAME;

        UPDATE dbrewind.metadata
        SET modified = TRUE
        WHERE table_name = TG_TABLE_NAME
        RETURNING dbrewind.metadata.unique_columns
        INTO unique_columns;

        unique_columns_row = dbrewind.format_columns('$1', unique_columns);

        -- We treat the table's unique columns as row identity, and the rest as row value.
        IF TG_OP IN ('DELETE', 'UPDATE') THEN
            unique_columns_diff_table = dbrewind.format_columns('diff_table', unique_columns);
            -- Record deleted - delete from the insertions diff table if present, otherwise add to the
            -- deletions diff table.
            -- It's important that the deletions table only ever holds records existing before
            -- dbrewind initialisation, and it always holds the oldest version of a record.
            EXECUTE format(
                'DELETE FROM dbrewind.%I AS diff_table WHERE (%s) = (%s)',
                insertions_table, unique_columns_row, unique_columns_diff_table
            ) USING OLD;

            GET DIAGNOSTICS deleted_count = ROW_COUNT;
            IF deleted_count = 0 THEN
                EXECUTE format('INSERT INTO dbrewind.%I SELECT $1.*', deletions_table)
                USING OLD;
            END IF;
        END IF;

        IF TG_OP IN ('INSERT', 'UPDATE') THEN
            -- Record inserted -  add to the insertions diff table.
            -- It is possible for the same row ID to be present in the deletions and the insertions
            -- tables simultaneously. This is how we encode an UPDATE. The non-ID values in the
            -- insertions table are irrelevant and may be skipped in the future.
            EXECUTE format(
                'INSERT INTO dbrewind.%I VALUES (%s) ON CONFLICT DO NOTHING',
                insertions_table, unique_columns_row)
            USING NEW;
        END IF;

        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;


-- Save progress of all public sequences.
CREATE OR REPLACE FUNCTION dbrewind.snapshot_sequences() RETURNS VOID AS $$
DECLARE seq_name TEXT;
BEGIN
    CREATE TABLE dbrewind.sequences (
        seq_name TEXT,
        last_value BIGINT,
        is_called BOOLEAN
    );

    FOR seq_name IN SELECT c.relname::text FROM pg_class c
    INNER JOIN pg_catalog.pg_namespace nsp ON nsp.oid = c.relnamespace
    WHERE c.relkind = 'S' AND nsp.nspname = 'public'
    LOOP
        EXECUTE format('INSERT INTO dbrewind.sequences (SELECT %L, last_value, is_called FROM %I)', seq_name, seq_name);
    END LOOP;
END;
$$ LANGUAGE plpgsql;


-- Set up the public schema for rewinding.
--
-- Create a pair of diff tables for each table in the public schema, install triggers on each public
-- table, and save index metadata.
--
-- By default, will also snapshot sequence state. Pass false to the unary variant to disable.
CREATE OR REPLACE FUNCTION dbrewind.install() RETURNS VOID AS $$
BEGIN
    PERFORM dbrewind.install(true);
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION dbrewind.install(BOOLEAN) RETURNS VOID AS $$
DECLARE restore_sequences ALIAS FOR $1;
        table_name TEXT;
        insertions_table TEXT;
        deletions_table TEXT;
        unique_columns TEXT[];
        unique_columns_str TEXT;
BEGIN
    CREATE UNLOGGED TABLE dbrewind.metadata (
        table_name TEXT NOT NULL PRIMARY KEY,
        unique_columns TEXT[] NOT NULL,
        modified BOOLEAN DEFAULT FALSE
    );

    FOR table_name IN SELECT tablename FROM pg_tables WHERE schemaname = 'public'
    LOOP
        insertions_table = 'records_inserted_' || table_name;
        deletions_table = 'records_deleted_' || table_name;

        unique_columns = dbrewind.unique_columns(table_name);
        unique_columns_str = dbrewind.format_columns(NULL, unique_columns);

        -- In future, OIDs could be used instead if present - they could even be added.
        IF unique_columns IS NULL THEN
            RAISE EXCEPTION 'INCOMPATIBLE SCHEMA: Table % doesn''t have a primary key or a unique constraint on non-nullable columns', table_name
            USING HINT = 'A defined uniqueness is required for rewinding to work.';
        END IF;

        EXECUTE format(
            'CREATE UNLOGGED TABLE dbrewind.%I AS SELECT %s FROM public.%I WITH NO DATA',
            insertions_table, unique_columns_str, table_name);
        EXECUTE format(
            'CREATE UNLOGGED TABLE dbrewind.%I AS TABLE public.%I WITH NO DATA',
            deletions_table, table_name);

        -- Add indices on unique column sets.
        EXECUTE format('ALTER TABLE dbrewind.%I ADD PRIMARY KEY (%s)', insertions_table, unique_columns_str);
        EXECUTE format('ALTER TABLE dbrewind.%I ADD PRIMARY KEY (%s)', deletions_table, unique_columns_str);

        EXECUTE format('CREATE TRIGGER dbrewind_row_modified
        AFTER INSERT OR UPDATE OR DELETE
        ON public.%I
        FOR EACH ROW
            EXECUTE PROCEDURE dbrewind.row_modified()', table_name);

        INSERT INTO dbrewind.metadata (table_name, unique_columns) VALUES (table_name, unique_columns);
    END LOOP;

    IF restore_sequences THEN
        PERFORM dbrewind.snapshot_sequences();
    END IF;
END;
$$ LANGUAGE plpgsql;


-- Undo dbrewind.install().
CREATE OR REPLACE FUNCTION dbrewind.uninstall() RETURNS VOID AS $$
DECLARE table_name TEXT;
BEGIN
    DROP TABLE IF EXISTS dbrewind.metadata;

    FOR table_name IN SELECT quote_ident(tablename) FROM pg_tables WHERE schemaname = 'public'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS dbrewind_row_modified ON public.%I', table_name);
    END LOOP;

    FOR table_name IN SELECT quote_ident(tablename) FROM pg_tables WHERE schemaname = 'dbrewind'
    LOOP
        EXECUTE format('DROP TABLE IF EXISTS dbrewind.%I', table_name);
    END LOOP;
END;
$$ LANGUAGE plpgsql;


-- Restore the state of public sequences according to the snapshot.
CREATE OR REPLACE FUNCTION dbrewind.restore_sequences() RETURNS VOID AS $$
BEGIN
    PERFORM setval('public.' || seq_name, last_value, is_called) FROM dbrewind.sequences;
END;
$$ LANGUAGE plpgsql;


-- Undo registered changes, since the call to dbrewind.install.
-- Requires superuser.
CREATE OR REPLACE FUNCTION dbrewind.rewind() RETURNS VOID AS $$
DECLARE table_name TEXT;
        unique_columns TEXT[];
        insertions_table TEXT;
        deletions_table TEXT;
        unique_columns_target TEXT;
        unique_columns_inserted TEXT;
BEGIN
    SET session_replication_role = 'replica';  -- Disable processing triggers and FK constraints.

    FOR table_name, unique_columns
    IN UPDATE dbrewind.metadata SET modified = FALSE
    WHERE modified IS TRUE
    RETURNING metadata.table_name, metadata.unique_columns
    LOOP
        insertions_table = 'records_inserted_' || table_name;
        deletions_table = 'records_deleted_' || table_name;

        unique_columns_target = dbrewind.format_columns('target', unique_columns);
        unique_columns_inserted = dbrewind.format_columns('inserted', unique_columns);

        -- Delete inserted
        EXECUTE format('
            DELETE FROM public.%I AS target
            USING dbrewind.%I AS inserted
            WHERE (%s) = (%s)',
            table_name, insertions_table, unique_columns_target, unique_columns_inserted
        );

        -- Insert deleted
        EXECUTE format('INSERT INTO public.%I (SELECT * FROM dbrewind.%I)', table_name, deletions_table);

        EXECUTE format('DELETE FROM dbrewind.%I; DELETE FROM dbrewind.%I', insertions_table, deletions_table);
    END LOOP;
    SET session_replication_role = 'origin'; -- Reenable triggers and FK constraints.

    -- The sequences table will only exist if a sequence snapshot was requested when installing dbrewind.
    IF to_regclass('dbrewind.sequences') IS NOT NULL
    THEN
        PERFORM dbrewind.restore_sequences();
    END IF;
END;
$$ LANGUAGE plpgsql;
