-- First, setup_user.sql and setup_db.sql must be run.
\pset pager 0
\c dbrewind_example

\i sql/dbrewind.sql
SELECT dbrewind.uninstall();
TRUNCATE public.sample;

BEGIN;

INSERT INTO public.sample VALUES (-1, 'init');
\echo ==== Initial state
SELECT 1 FROM dbrewind.install();
SELECT * FROM dbrewind.metadata;
SELECT * FROM public.sample;
SELECT * FROM dbrewind.records_inserted_sample;
SELECT * FROM dbrewind.records_deleted_sample;

\echo ==== Changes
INSERT INTO public.sample (col1) VALUES ('1'), ('2'), ('3'), ('4'), ('5'), ('6');
DELETE FROM public.sample WHERE col1 IN ('3', '4');
INSERT INTO public.sample (col1) VALUES ('11'), ('22'), ('33'), ('44'), ('55'), ('66');
DELETE FROM public.sample WHERE col1 IN ('33', '44');

\echo ==== Changes - outcome:
SELECT * FROM public.sample;
SELECT * FROM dbrewind.records_inserted_sample;
SELECT * FROM dbrewind.records_deleted_sample;

\echo Rewinding
SELECT 1 FROM dbrewind.rewind();

\echo ==== Rewinding - outcome:
SELECT * FROM public.sample;
SELECT * FROM dbrewind.records_inserted_sample;
SELECT * FROM dbrewind.records_deleted_sample;

ROLLBACK;
