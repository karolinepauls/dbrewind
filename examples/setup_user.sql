CREATE USER dbrewind_test WITH PASSWORD 'dbrewind';
ALTER USER dbrewind_test WITH LOGIN;
ALTER USER dbrewind_test WITH SUPERUSER;
