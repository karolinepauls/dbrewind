-- First, setup_user.sql and setup_db.sql must be run.
\pset pager 0
\c dbrewind_example

\i sql/dbrewind.sql
SELECT dbrewind.uninstall();
TRUNCATE public.sample_unique;

BEGIN;

INSERT INTO public.sample_unique (col1, col2) VALUES ('initial', 'aaaa');
\echo ==== Initial state
SELECT 1 FROM dbrewind.install();
SELECT * FROM dbrewind.metadata;
SELECT * FROM public.sample_unique;
SELECT * FROM dbrewind.records_inserted_sample_unique;
SELECT * FROM dbrewind.records_deleted_sample_unique;

SELECT * FROM dbrewind.metadata;
INSERT INTO public.sample_unique (col1, col2) SELECT 'val-' || generate_series, 'b' FROM generate_series(1, 10000);

\echo ==== Changes
INSERT INTO public.sample_unique (col1, col2) VALUES ('aa', 'a'), ('bb', 'a');
DELETE FROM public.sample_unique WHERE (col1, col2) = ('bb', 'a');
INSERT INTO public.sample_unique (col1, col2) VALUES ('cc', 'a');

\echo ==== Changes - outcome:
SELECT * FROM public.sample_unique;
SELECT * FROM dbrewind.records_inserted_sample_unique;
SELECT * FROM dbrewind.records_deleted_sample_unique;

\echo Rewinding
SELECT 1 from dbrewind.rewind();

SELECT * FROM public.sample_unique;
SELECT * FROM dbrewind.records_inserted_sample_unique;
SELECT * FROM dbrewind.records_deleted_sample_unique;

ROLLBACK;
