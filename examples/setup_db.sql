-- Database setup for examples.
CREATE DATABASE dbrewind_example;

\c dbrewind_example
CREATE TABLE public.sample (
    id SERIAL PRIMARY KEY,
    col1 TEXT
);
INSERT INTO public.sample (col1) VALUES (NULL), ('abc');

CREATE TABLE public.sample_unique (
    col1 TEXT NOT NULL,
    col2 TEXT NOT NULL,
    CONSTRAINT uniq UNIQUE(col1, col2)
);
INSERT INTO public.sample_unique (col1, col2) VALUES ('a', 'b'), ('A', 'B');
